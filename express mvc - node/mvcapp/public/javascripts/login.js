$(document).ready(function () {
    var users = [];
    users.push(new User("first", "qwerty"));
    users.push(new User("root", "hgu77wqq"));
    $("#username").keyup(function () {
        for (var i = 0; i < users.length; i++)
            if(users[i].username == $("#username").val()) {$("#username").css("border", "1px solid rgba(0,255,0,0.9)"); break;}
            else $("#username").css("border", "1px solid rgba(255,0,0,0.9)");
    });
    $("#LoginButton").click(function () {
        var username = $("#username").val();
        var password = $("#password").val();
        switch(username){
            case "first":
                    $("#username").css("border", "1px solid rgba(0,255,0,0.9)");
                    if (password == "qwerty")
                        $(".grad").slideUp(700, function() {
                            window.location.href = "/info?username=" + username;
                        });
                    else $("#password").css("border", "1px solid rgba(255,0,0,0.9)");
                break;
            case "root":
                    $("#username").css("border", "1px solid rgba(0,255,0,0.9)");
                    if (password == "hgu77wqq")                        
                        $(".grad").slideUp(700, function() {
                            window.location.href = "/info?username=" + username;
                        });
                    else $("#password").css("border", "1px solid rgba(255,0,0,0.9)");
                break;
            default:
                    $("#username").css("border", "1px solid rgba(255,0,0,0.9)");
                break;
        }            
    });
});
function User (username, password) {
    this.username = username;
    this.password = password;
}
