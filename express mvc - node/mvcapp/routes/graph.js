var express = require('express');
var router = express.Router();

var app = express(),
               server = require('http').createServer(app),
	io = require('socket.io').listen(server),
	spawn = require('child_process').spawn;

server.listen(8081);

app.get('/', function (req, res) { 
  res.sendfile('views/index.html');
});

var com = spawn('dstat', ['-c', '--nocolor']);
com.stdout.on('data', function(data){
	var txt = new Buffer(data).toString('utf8', 0, data.length);
	io.sockets.send(parseInt(txt.split('  ')[2]));
});

module.exports = router;
