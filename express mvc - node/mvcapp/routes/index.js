var express = require('express');
var router = express.Router();

var info = [];
var exec = require('child_process').exec;
exec("ifconfig | grep 195",
  function (error, stdout, stderr) {
    info.push(stdout.substring(stdout.indexOf("195"), stdout.indexOf("195") + 13));
    });
exec("hostname",
  function (error, stdout, stderr) {
    info.push(stdout);
    });

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'MS4S', username: req.url.substring(req.url.indexOf("username=") + 9), inf: info[2], hostname: info[1]});
});

module.exports = router;
